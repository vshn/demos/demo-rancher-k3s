#!/usr/bin/env bash

HOSTNAME=rancher-demo.eu.ngrok.io

# Install cert-manager
helm --kubeconfig k3s.kubeconfig install cert-manager jetstack/cert-manager --namespace cert-manager --create-namespace --version v1.3.1 --set installCRDs=true
kubectl --kubeconfig k3s.kubeconfig -n cert-manager rollout status deploy/cert-manager
echo "Waiting for cert-manager-webhook"
sleep 10

# Install Rancher
helm --kubeconfig k3s.kubeconfig install rancher rancher-latest/rancher --namespace cattle-system --create-namespace --set hostname=$HOSTNAME
kubectl --kubeconfig k3s.kubeconfig -n cattle-system rollout status deploy/rancher
echo "Waiting for Rancher to be ready"
sleep 20

# Print the default admin password
RANCHER_PWD=$(kubectl get secret --namespace cattle-system bootstrap-secret -o go-template='{{.data.bootstrapPassword|base64decode}}{{"\n"}}')
echo "Default Rancher password: $RANCHER_PWD"

# Start ngrok exposing Rancher to the outer world
URL=$(kubectl --kubeconfig k3s.kubeconfig get ingress --namespace cattle-system | grep rancher | awk '{ print $4 }' | cut -d "," -f 1)
echo "Start an ngrok session to Rancher using this command:"
echo "ngrok http --region=eu --hostname=$HOSTNAME https://$URL"

